# Willkommen bei unserer Abgabe für SWE
Alexander Mader, Matthias Treise, Marius Gulden

## Einleitung

Dies ist die erste Abgabe für das Fach Software Engineering an der Hochschule Karlsruhe (H-KA) für das Sommersemester 2021 bei Professor Dr. Jürgen Zimmerman. 

## Unserer Projekt

Dieses Projekt ist Server auf der Basis von NodeJs mit Kompenten aus dem MEAN Umfeld. Zusätzlich zu einer klassischen REST-Schnittstelle, stellen wir auch eine GraphQL-Schnittstelle mit Playground zur Verfügung. Zur Optimierung der Codequalität und Sicherheit verwenden wir SonarQube, Eslint, Prettier und Snyk. 
Das Projekt ist in der Kommandozeile lauffähig, es wird aber über ein Dockerfile und Skaffold auch eine Docker Image sowie ein Kubernetes Deployment ermöglicht.

## GitLab

Dieses Projekt wird auf GitLab gehostet. Sie finden alle nötigen Files hier: https://gitlab.com/L3XAWeasel/swess2021abgabe1

## Heroku

Zusätzlich zu Docker und Kubernetes gibt es eine Deployment in Heroku. Dieses wird durch eine CD-Pipeline aus GitLab heraus erzeugt und aktuell gehalten. Die URL für dieses Deployment ist: https://filmswe.herokuapp.com/
Es gibt keine HTML-Schnittstelle, jedoch ist der Playground für GraphQL aktiv.

## Jenkins

Außerdem wird das Projekt durch eine an unsere Git-Verzeichnis gekoppelte Jenkins Pipeline überwacht.
Diese läuft bei uns lokal in einem Docker Container.